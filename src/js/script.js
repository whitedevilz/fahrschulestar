new fullpage('#fullpage', {
    // navigation: true,
    responsiveWidth: 200,
    parallax: true,
    scrollBar: true,
});


// sidenav
function openNav() {
    document.getElementById("Sidenav").style.width = "300px";
}

function closeNav() {
    document.getElementById("Sidenav").style.width = "0";
}


$(document).ready(function () {

    const vh = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0);

    $(window).scroll(function () {
        if ($(this).scrollTop() <= 500) {
            $('h1').html('Willkommen bei Fahrschule Star').fadeIn(500);
            $('p').html('Auto fahren leicht gemacht');
            $('.content-btn-dark').html('BUCHEN');
            $('.content-btn-dark').attr("href", "https://fahrschulestar.ch/fahrlektionen");
            $('.content-btn-danger').html('ÜBER UNS');
            $('a.content-btn-danger').attr("href", "https://fahrschulestar.ch/about");
        } else if ($(this).scrollTop() > vh - 10 && $(this).scrollTop() < vh * 2 - 10) {
            $('h1').html('Lerne Auto fahren und die Verkehrsregeln');
            $('p').html('Super Fahrlehrer für super Fahrschüler');
            $('.content-btn-dark').html('BUCHEN');
            $('.content-btn-dark').attr("href", "https://fahrschulestar.ch/fahrlektionen");
            $('.content-btn-danger').html('MEHR');
            $('a.content-btn-danger').attr("href", "https://fahrschulestar.ch/fahrlektionen");
        } else if ($(this).scrollTop() > vh * 2 - 10 && $(this).scrollTop() < vh * 3 - 10) {
            $('h1').html('Nothelfer');
            $('p').html('Buche deinen Termin für den Nothelferkurs bequem online');
            $('.content-btn-dark').html('BUCHEN');
            $('.content-btn-dark').attr("href", "https://fahrschulestar.ch/book");
            $('.content-btn-danger').html('MEHR');
            $('a.content-btn-danger').attr("href", "https://fahrschulestar.ch/nothilfekurs");
        } else if ($(this).scrollTop() > vh * 3 - 10 && $(this).scrollTop() < vh * 4 - 10) {
            $('h1').html('VKU');
            $('p').html('Buche deine Termine für den Verkehrskundeunterricht (VKU) in deiner Region');
            $('.content-btn-dark').html('BUCHEN');
            $('.content-btn-dark').attr("href", "https://fahrschulestar.ch/book");
            $('.content-btn-danger').html('MEHR');
            $('a.content-btn-danger').attr("href", "https://fahrschulestar.ch/verkehrskundeunterricht");
        } else if ($(this).scrollTop() > vh * 4 - 10 && $(this).scrollTop() < vh * 5 - 10) {
            $('h1').html('Motorrad');
            $('p').html('Motorradkurs für die Region Aarau, Baden und Zürich');
            $('.content-btn-dark').html('BUCHEN');
            $('.content-btn-dark').attr("href", "https://fahrschulestar.ch/book");
            $('.content-btn-danger').html('MEHR');
            $('a.content-btn-danger').attr("href", "https://fahrschulestar.ch/grundkurs");
        } else if ($(this).scrollTop() > vh * 5 - 10) {
            $('h1').html('Preise');
            $('p').html('Heute fahren – morgen zahlen');
            $('a.content-btn-dark').html('BUCHEN');
            $('a.content-btn-dark').attr("href", "https://fahrschulestar.ch/fahrlektionen");
            $('a.content-btn-danger').html('MEHR');
            $('a.content-btn-danger').attr("href", "https://fahrschulestar.ch/preise");
        }
    });
});



$(function () {
    var element = $('.middle');
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            element.fadeOut(100);
        } else {
            element.fadeIn(100);
        }
    });
});